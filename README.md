<p align="center"><img src="https://gitlab.com/NovumGit/innovation-app-core/-/raw/master/assets/novum.png"  alt="Novum logo"/></p>

# Schema files for Novum Innovatie App.

## What
This repository contains the schema files needed for code generation and 
datamodel initialization of the Novum Innovation App. These schema files are
based on Propel ORM but modified and extended to support additional features.

## Who
People that are installing the actual system will make use of these files 
indirectly. Unless you would like to alter the behavior of the system you can
probably forget these files.

# More info
- [Main project](https://gitlab.com/NovumGit/innovation-app-core) 
- [demo system](https://gitlab.com/NovumGit/innovation-app-demo) 
- [Propel ORM](https://github.com/propelorm/Propel) 

# Reference these files
These files are hosted via Gitlab pages, you can reference them by appending the required file to this url
- [Gitlab page root](https://novumgit.gitlab.io/innovation-app-schema-xsd)
